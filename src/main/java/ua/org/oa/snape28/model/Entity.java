package ua.org.oa.snape28.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Entity
 * @param <T>
 * @author Svyatoslav Kotlyar.
 */
@Getter @Setter
public class Entity<T> {
    /**
     * Id of entity.
     */
    private T id;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;

        Entity<?> entity = (Entity<?>) o;

        return getId().equals(entity.getId());

    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
