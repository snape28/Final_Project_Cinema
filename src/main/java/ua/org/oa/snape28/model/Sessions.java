package ua.org.oa.snape28.model;

import lombok.Data;

import java.sql.Timestamp;


/**
 * Session description.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Sessions extends Entity<Integer> {
    /**
     * Object film.
     */
    private Movies film;
    /**
     * Session time.
     */
    private Timestamp timeOfSession;
    /**
     * model.Tickets price.
     */
    /**
     * Object hall.
     */
    private Halls halls;

    /**
     * Constructor.
     *
     * @param film          Object film.
     * @param timeOfSession Session time.
     * @param halls         Object hall.
     */
    public Sessions(Movies film, Timestamp timeOfSession, Halls halls) {
        this.film = film;
        this.timeOfSession = timeOfSession;
        this.halls = halls;
    }

    /**
     * Constructor.
     */
    public Sessions() {
    }

}



