package ua.org.oa.snape28.model;

import lombok.Data;

/**
 * Film description.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Movies extends Entity<Integer> {

    /**
     * Film name.
     */
    private String name;
    /**
     * Film description.
     */
    private String description;

    /**
     * Duration of movie;
     */
    private Long duration;


    /**
     * Constructor.
     *
     * @param name        Film name.
     * @param description Film description.
     * @param duration    Duration of movie;
     */
    public Movies(String name, Long duration, String description) {
        this.name = name;
        this.description = description;
        this.duration = duration;
    }

    /**
     * Constructor.
     */
    public Movies() {
    }
}

