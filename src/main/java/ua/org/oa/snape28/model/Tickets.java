package ua.org.oa.snape28.model;

import lombok.Data;


/**
 * Ticket description.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Tickets extends Entity<Integer> {
    /**
     * Film session.
     */
    private Sessions session;
    /**
     * Place.
     */
    private Places place;
    /**
     * User with buy ticket.
     */
    private Users user;
    /**
     * Price of ticket.
     */
    private int price;

    /**
     * Constructor.
     *
     * @param session Film session.
     * @param places  Place.
     * @param user    User which buy ticket.
     * @param price   Price of ticket.
     */
    public Tickets(Sessions session, Places places, Users user, int price) {
        this.session = session;
        this.place = places;
        this.user = user;
        this.price = price;
    }

    /**
     * Constructor.
     */
    public Tickets() {
    }
}
