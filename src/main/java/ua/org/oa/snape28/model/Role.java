package ua.org.oa.snape28.model;

import lombok.Data;


/**
 * Role.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Role extends Entity<Integer> {

    /**
     * Name of role.
     */
    private String role;

    /**
     * Constructor.
     */
    public Role() {
    }

    public Role(Integer id) {
        if (id == 2) {
            this.role = "customer";
            this.setId(2);
        }
    }

    /**
     * Gets role.
     * @return role.
     */
    public String getRole() {
        return role;
    }

    /**
     * Set role.
     * @param role role.
     */
    public void setRole(String role) {
        this.role = role;
    }
}


