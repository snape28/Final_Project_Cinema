package ua.org.oa.snape28.model;

import lombok.Data;

/**
 * Hall description.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Halls extends Entity<Integer> {
    /**
     * Name of hall.
     */
    private String name;

    /**
     * Constructor.
     *
     * @param name        Name of hall.
     */
    public Halls(String name) {
        this.name = name;
    }

    /**
     * Constructor.
     */
    public Halls() {
    }
}
