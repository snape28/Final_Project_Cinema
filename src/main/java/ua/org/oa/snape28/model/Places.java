package ua.org.oa.snape28.model;

import lombok.Data;

/**
 * Places description.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Places extends Entity<Integer> {
    /**
     * Hall.
     */
    private Halls hall;
    /**
     * Row number.
     */
    private int row;
    /**
     * Row number.
     */
    private int placeNumber;

    /**
     * Constructor.
     *
     * @param hall
     * @param row
     * @param placeNumber
     */
    public Places(Halls hall, int row, int placeNumber) {
        this.hall = hall;
        this.row = row;
        this.placeNumber = placeNumber;
    }

    /**
     * Constructor.
     */
    public Places() {
    }

}
