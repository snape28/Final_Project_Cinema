package ua.org.oa.snape28.model;

import lombok.Data;


/**
 * User.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class Users extends Entity<Integer> {
    /**
     * User login
     */
    private String login;
    /**
     * User password.
     */
    private String password;
    /**
     * User mail.
     */
    private String mail;
    /**
     * Role of user.
     */
    private Role role;

    /**
     * Constructor.
     *
     * @param login    User login
     * @param password User password.
     * @param mail     User mail.
     */
    public Users(String login, String password, String mail, Role role) {
        this.login = login;
        this.password = password;
        this.mail = mail;
        this.role = role;
    }

    /**
     * Constructor.
     */
    public Users() {
    }

    /**
     * Sets role.
     * @param role role.
     */
    public void setRole(Role role) {
        this.role = role;
    }
}

