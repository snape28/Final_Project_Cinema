package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.datasource.DataSource;
import ua.org.oa.snape28.model.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * CRUD dao implementation
 *
 * @author Svyatoslav Kotlyar.
 */
public abstract class CrudDAO<T extends Entity<Integer>> implements Dao<Integer, T> {

    private Class<T> type;
    private DataSource dataSource;

    public static final String SELECT_ALL = "Select * from %s";
    public static final String FIND_BY_ID = "Select * from %s where id = ?";
    public static final String DELETE_BY_ID = "DELETE FROM %s WHERE id = ?";
    public static final String FIND_BY = "Select * from %s where %s = ?";


    public CrudDAO(Class<T> type) {
        this.type = type;
        dataSource = DataSource.getInstance();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getAll() {
        String sql = String.format(SELECT_ALL, type.getSimpleName()).toLowerCase();
        List result = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public T getById(Integer id) {
        String sql = String.format(FIND_BY_ID, type.getSimpleName().toLowerCase());
        List result = null;
        try (Connection connection = dataSource.getConnection();) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (T) result.get(0);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public T getBy(String fieldName, String value) {
        String sql = String.format(FIND_BY, type.getSimpleName().toLowerCase(), fieldName);
        List result = null;
        try (Connection connection = dataSource.getConnection();) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, value);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (result.size() > 0) ? (T) result.get(0) : null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void save(T entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = createInsertStatement(connection, entity)) {
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void update(T entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = createUpdateStatement(connection, entity)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Integer key) {
        String sql = String.format(DELETE_BY_ID, type.getSimpleName()).toLowerCase();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getAllByFieldId(String fieldName, Integer key) {
        String sql = String.format(FIND_BY, type.getSimpleName().toLowerCase(), fieldName);
        List resultList = null;
        try (Connection connection = dataSource.getConnection();) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultList = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    /**
     * Creates update statement.
     * @param connection Connection.
     * @param entity Entity which need to update.
     * @return PreparedStatement
     * @throws SQLException in case incorrect SQL syntax's.
     */
    protected abstract PreparedStatement createUpdateStatement(Connection connection, T entity) throws SQLException;

    /**
     * Creates insert statement.
     * @param connection Connection.
     * @param entity Entity which need to insert.
     * @return PreparedStatement
     * @throws SQLException in case incorrect SQL syntax's.
     */
    protected abstract PreparedStatement createInsertStatement(Connection connection, T entity) throws SQLException;

    /**
     * Reads all objects.
     * @param resultSet ResultSet.
     * @return List of objects which was read from database.
     * @throws SQLException in case incorrect SQL syntax's.
     */
    protected abstract List<T> readAll(ResultSet resultSet) throws SQLException;

}
