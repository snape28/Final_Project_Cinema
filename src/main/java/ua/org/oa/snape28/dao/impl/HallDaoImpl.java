package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.model.Halls;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Hall dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public class HallDaoImpl extends CrudDAO<Halls> {
    private final String INSERT = "Insert into halls (name) values (?)";
    private final String UPDATE = "UPDATE halls SET name = ?, WHERE id = ?";
    private static HallDaoImpl crudDAO;

    public HallDaoImpl(Class type) {
        super(type);
    }

    public static synchronized HallDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new HallDaoImpl(Halls.class);
        }
        return crudDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Halls entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getId());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Halls entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Halls> readAll(ResultSet resultSet) throws SQLException {
        List<Halls> result = new LinkedList<>();
        Halls halls = null;
        while (resultSet.next()) {
            halls = new Halls();
            halls.setId(resultSet.getInt("id"));
            halls.setName(resultSet.getString("name"));
            result.add(halls);
        }
        return result;
    }
}
