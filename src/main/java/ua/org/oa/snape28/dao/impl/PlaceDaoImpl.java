package ua.org.oa.snape28.dao.impl;

import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.model.Places;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Places dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public class PlaceDaoImpl extends CrudDAO<Places> {
    private final String INSERT = "Insert into places (hall_id, row_number, place_number) values (?,?,?)";
    private final String UPDATE = "UPDATE places SET hall_id = ?, row_number = ?, place_number = ?, WHERE id = ?";
    private static PlaceDaoImpl crudDAO;

    public PlaceDaoImpl(Class type) {
        super(type);
    }

    public static synchronized PlaceDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new PlaceDaoImpl(Places.class);
        }
        return crudDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Places entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getHall().getId());
        preparedStatement.setInt(2, entity.getRow());
        preparedStatement.setInt(3, entity.getPlaceNumber());
        preparedStatement.setInt(4, entity.getId());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Places entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getHall().getId());
        preparedStatement.setInt(2, entity.getRow());
        preparedStatement.setInt(3, entity.getPlaceNumber());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Places> readAll(ResultSet resultSet) throws SQLException {
        List<Places> result = new LinkedList<>();
        Places places = null;
        while (resultSet.next()) {
            places = new Places();
            places.setId(resultSet.getInt("id"));
            places.setHall(DaoFactory.getInstance().getHallsDao().getById(resultSet.getInt("hall_id")));
            places.setRow(resultSet.getInt("row_number"));
            places.setPlaceNumber(resultSet.getInt("place_number"));
            result.add(places);
        }
        return result;
    }
}
