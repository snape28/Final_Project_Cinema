package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.model.Tickets;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Tickets dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public class TicketDaoImpl extends CrudDAO<Tickets> {
    private final String INSERT = "Insert into tickets (session_id, place_id, ticket_price) values (?,?,?)";
    private final String UPDATE = "UPDATE tickets SET session_id = ?, place_id = ?, user_id = ?, ticket_price = ?  WHERE id = ?";


    private static TicketDaoImpl crudDAO;

    public TicketDaoImpl(Class type) {
        super(type);
    }

    public static synchronized TicketDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new TicketDaoImpl(Tickets.class);
        }
        return crudDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Tickets entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getSession().getId());
        preparedStatement.setInt(2, entity.getPlace().getId());
        preparedStatement.setInt(3, entity.getUser().getId());
        preparedStatement.setInt(4, entity.getPrice());
        preparedStatement.setInt(5, entity.getId());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Tickets entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getSession().getId());
        preparedStatement.setInt(2, entity.getPlace().getId());
        //preparedStatement.setInt(3, entity.getUser().getId());
        preparedStatement.setInt(3, entity.getPrice());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Tickets> readAll(ResultSet resultSet) throws SQLException {
        List<Tickets> result = new LinkedList<>();
        Tickets tickets = null;
        while (resultSet.next()) {
            tickets = new Tickets();
            tickets.setId(resultSet.getInt("id"));
            tickets.setSession(DaoFactory.getInstance().getSessionDao().getById(resultSet.getInt("session_id")));
            tickets.setPlace(DaoFactory.getInstance().getPlaceDao().getById(resultSet.getInt("place_id")));
            int userId = resultSet.getInt("user_id");
            // User id for not saled tickets is null in sql, int value is 0.
            if (userId != 0)
                tickets.setUser(DaoFactory.getInstance().getUserDao().getById(userId));
            tickets.setPrice(resultSet.getInt("ticket_price"));
            result.add(tickets);
        }
        return result;
    }
}
