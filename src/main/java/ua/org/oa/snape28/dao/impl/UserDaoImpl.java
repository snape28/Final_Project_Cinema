package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.model.Users;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * User dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public class UserDaoImpl extends CrudDAO<Users> {
    private final String INSERT = "Insert into users (login, password, mail, role_id) values (?,?,?,?)";
    private final String UPDATE = "UPDATE users SET login = ?, password = ?, mail = ?, role_id =?, WHERE id = ?";
    private static UserDaoImpl crudDAO;


    public UserDaoImpl(Class type) {
        super(type);
    }

    public static synchronized UserDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new UserDaoImpl(Users.class);
        }
        return crudDAO;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Users entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPassword());
        preparedStatement.setString(3, entity.getMail());
        preparedStatement.setInt(4, entity.getRole().getId());
        preparedStatement.setInt(5, entity.getId());
        return preparedStatement;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Users entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPassword());
        preparedStatement.setString(3, entity.getMail());
        preparedStatement.setInt(4, entity.getRole().getId());
        return preparedStatement;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Users> readAll(ResultSet resultSet) throws SQLException {
        List<Users> result = new LinkedList<>();
        Users users = null;
        while (resultSet.next()) {
            users = new Users();
            users.setId(resultSet.getInt("id"));
            users.setLogin(resultSet.getString("login"));
            users.setPassword(resultSet.getString("password"));
            users.setMail(resultSet.getString("mail"));
            users.setRole(DaoFactory.getInstance().getRoleDao().getById(resultSet.getInt("role_id")));
            result.add(users);
        }
        return result;
    }
}
