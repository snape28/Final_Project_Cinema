package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.datasource.DataSource;
import ua.org.oa.snape28.model.Sessions;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Session dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public class SessionDaoImpl extends CrudDAO<Sessions> {
    private final String INSERT = "Insert into sessions (movie_id, time_of_start, hall_id) values (?,?,?)";
    private final String UPDATE = "UPDATE sessions SET movie_id = ?, time_of_start = ?, hall_id = ?, WHERE id = ?";


    private static SessionDaoImpl crudDAO;
    private DataSource dataSource;

    public SessionDaoImpl(Class type) {
        super(type);
    }


    public static synchronized SessionDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new SessionDaoImpl(Sessions.class);
        }
        return crudDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Sessions entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getFilm().getId());
        preparedStatement.setTimestamp(2, entity.getTimeOfSession());
        preparedStatement.setInt(3, entity.getHalls().getId());
        preparedStatement.setInt(4, entity.getId());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Sessions entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getFilm().getId());
        preparedStatement.setTimestamp(2, entity.getTimeOfSession());
        preparedStatement.setInt(3, entity.getHalls().getId());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Sessions> readAll(ResultSet resultSet) throws SQLException {
        List<Sessions> result = new LinkedList<>();
        Sessions sessions = null;
        while (resultSet.next()) {
            sessions = new Sessions();
            sessions.setId(resultSet.getInt("id"));
            sessions.setFilm(DaoFactory.getInstance().getMovieDao().getById(resultSet.getInt("movie_id")));
            sessions.setTimeOfSession(resultSet.getTimestamp("time_of_start"));
            sessions.setHalls(DaoFactory.getInstance().getHallsDao().getById(resultSet.getInt("hall_id")));
            result.add(sessions);
        }
        return result;
    }

}



