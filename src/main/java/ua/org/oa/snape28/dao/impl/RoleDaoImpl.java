package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.model.Role;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Role dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public class RoleDaoImpl extends CrudDAO<Role> {
    public static final String INSERT_ROLE = "Insert into role (role) values (?)";
    public static final String UPDATE_ROLE = "UPDATE role SET role = ?";
    private static RoleDaoImpl crudDAO;

    public RoleDaoImpl(Class type) {
        super(type);
    }

    public static synchronized RoleDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new RoleDaoImpl(Role.class);
        }
        return crudDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Role entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROLE);
        preparedStatement.setString(1, entity.getRole());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Role entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROLE, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getRole());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Role> readAll(ResultSet resultSet) throws SQLException {
        List<Role> result = new LinkedList<>();
        Role role = null;
        while (resultSet.next()) {
            role = new Role();
            role.setId(resultSet.getInt("id"));
            role.setRole(resultSet.getString("role"));
            result.add(role);
        }
        return result;
    }
}
