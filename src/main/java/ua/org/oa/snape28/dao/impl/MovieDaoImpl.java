package ua.org.oa.snape28.dao.impl;


import ua.org.oa.snape28.model.Movies;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Movie dao implementation, which includes:
 * creates insert and update statement and read all objects from database..
 *
 * @author Svyatoslav Kotlyar.
 */
public final class MovieDaoImpl extends CrudDAO<Movies> {
    private final String INSERT = "Insert into movies (name, duration, description) values (?,?,?)";
    private final String UPDATE = "UPDATE movies SET name = ?, duration = ?, description = ?";
    private static MovieDaoImpl crudDAO;

    public MovieDaoImpl(Class type) {
        super(type);
    }


    public static synchronized MovieDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new MovieDaoImpl(Movies.class);
        }
        return crudDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movies entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setLong(2, entity.getDuration());
        preparedStatement.setString(3, entity.getDescription());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PreparedStatement createInsertStatement(Connection connection, Movies entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setLong(2, entity.getDuration());
        preparedStatement.setString(3, entity.getDescription());
        return preparedStatement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Movies> readAll(ResultSet resultSet) throws SQLException {
        List<Movies> result = new LinkedList<>();
        Movies movies = null;
        while (resultSet.next()) {
            movies = new Movies();
            movies.setId(resultSet.getInt("id"));
            movies.setName(resultSet.getString("name"));
            movies.setDuration(resultSet.getLong("duration"));
            movies.setDescription(resultSet.getString("description"));
            result.add(movies);
        }
        return result;
    }
}