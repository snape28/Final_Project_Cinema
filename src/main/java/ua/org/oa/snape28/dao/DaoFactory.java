package ua.org.oa.snape28.dao;


import lombok.Getter;
import lombok.Setter;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dao.impl.*;
import ua.org.oa.snape28.helper.PropertyHolder;
import ua.org.oa.snape28.model.*;

/**
 * Dao factory
 *
 * @author Svyatoslav Kotlyar.
 */
@Getter
@Setter
public class DaoFactory {

    private static DaoFactory instance = null;

    private Dao<Integer, Movies> movieDao;
    private Dao<Integer, Users> userDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Sessions> sessionDao;
    private Dao<Integer, Halls> hallsDao;
    private Dao<Integer, Tickets> ticketsDao;
    private Dao<Integer, Places> placeDao;

    private DaoFactory() {
        loadDaos();
    }

    public static DaoFactory getInstance() {
        if (instance == null) {
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
        if (PropertyHolder.getInstance().isInMemoryDB()) {
        } else {
            movieDao = new MovieDaoImpl(Movies.class);
            userDao = new UserDaoImpl(Users.class);
            roleDao = new RoleDaoImpl(Role.class);
            sessionDao = new SessionDaoImpl(Sessions.class);
            hallsDao = new HallDaoImpl(Halls.class);
            ticketsDao = new TicketDaoImpl(Tickets.class);
            placeDao = new PlaceDaoImpl(Places.class);
        }
    }
}
