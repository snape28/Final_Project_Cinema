package ua.org.oa.snape28.controllers.Admin;



import ua.org.oa.snape28.dto.MovieDTO;
import ua.org.oa.snape28.service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a controller which process add movie page by admin.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "AddMovieServlet", urlPatterns = "/admin_add_movie")
public class AddMovieServlet extends HttpServlet {

    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String duration = request.getParameter("duration");
        request.setCharacterEncoding("UTF-8");

        if (isDataValid(name, description, duration)) {

            MovieDTO movie = new MovieDTO();
            movie.setName(name);
            movie.setDescription(description);
            movie.setDuration(Integer.parseInt(duration));

            MovieServiceImpl.getInstance().save(movie);

            request.getSession().setAttribute("message", "New film was succesfully added.");
        } else {
            request.getSession().setAttribute("message", "Invalid movie information.");
        }
        response.sendRedirect("admin_movies");
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Checks is parameters from request valid or not.
     * @param name Name of movie.
     * @param descripton Description of movie.
     * @param duration Duration of movie.
     * @return boolean result: true in valid case and false in invalid.
     */
    private boolean isDataValid(String name, String descripton, String duration) {
        boolean isDurationValid = true;

        try {
            int filmDuration = Integer.parseInt(duration);
        } catch (Exception e) {
            isDurationValid = false;
        }
        return (name != null && !name.isEmpty())
                && (descripton != null && !descripton.isEmpty())
                && isDurationValid;
    }
}
