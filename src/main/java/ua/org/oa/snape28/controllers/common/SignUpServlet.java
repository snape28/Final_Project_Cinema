package ua.org.oa.snape28.controllers.common;



import ua.org.oa.snape28.dto.RoleDTO;
import ua.org.oa.snape28.dto.UserDTO;
import ua.org.oa.snape28.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a controller which process sign up page.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "SignUpServlet", urlPatterns = "/signup")
public class SignUpServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = null;
        String password = null;
        String mail = null;
        login = request.getParameter("login");
        password = request.getParameter("password");
        mail = request.getParameter("mail");

        UserDTO userDTO = UserServiceImpl.getInstance().getByLogin(login);

        if (userDTO != null) {
            request.getSession().setAttribute("message", "This user name is already exist! Choose another user name!");
            request.getRequestDispatcher("/pages/common/signup.jsp").forward(request, response);
        } else if (login == null || login.length() < 6 || mail == null || mail.length() < 6 || password == null || password.length() < 6) {
            request.getSession().setAttribute("message", "Wrong format of user name, password or mail! Check it, they length must be bigger then 6! ");
            request.getRequestDispatcher("/pages/common/signup.jsp").forward(request, response);
        } else {
            userDTO = new UserDTO();
            userDTO.setLogin(login);
            userDTO.setPassword(password);
            userDTO.setMail(mail);
            userDTO.setRole(new RoleDTO(2));
            UserServiceImpl.getInstance().save(userDTO);
            request.getSession().setAttribute("user", userDTO);
            request.getRequestDispatcher("pages/common/after_registration.jsp").forward(request, response);
        }
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}