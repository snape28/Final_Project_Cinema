package ua.org.oa.snape28.controllers.common;



import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a controller which process output page of information about selected ticket..
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "TicketServlet", urlPatterns = "/ticket")
public class TicketServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getAttribute("id");
        TicketDTO ticketDTO = TicketServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));

        request.setAttribute("ticketDTO", ticketDTO);

        request.getRequestDispatcher("pages/common/buy_ticket.jsp").forward(request, response);
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
