package ua.org.oa.snape28.controllers.User;



import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.dto.UserDTO;
import ua.org.oa.snape28.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process output page of users tickets.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "UserTicketsServlet", urlPatterns = "/usersTicket")
public class UserTicketsServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        List<TicketDTO> userTickets = TicketServiceImpl.getInstance().getAllByFieldId("user_id", user.getId());
        request.getSession().setAttribute("userTickets", userTickets);
        request.getRequestDispatcher("pages/common/user_tickets.jsp").forward(request, response);
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
