package ua.org.oa.snape28.controllers.common;


import ua.org.oa.snape28.dto.MovieDTO;
import ua.org.oa.snape28.dto.SessionDTO;
import ua.org.oa.snape28.service.impl.MovieServiceImpl;
import ua.org.oa.snape28.service.impl.SessionServiseImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process movie description page.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "MovieServlet", urlPatterns = "/movie")
public class MovieServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int movieId = Integer.parseInt(request.getParameter("id"));

        List<SessionDTO> sessions = SessionServiseImpl.getInstance().getAllByFieldId("movie_id", movieId);

        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(movieId);
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("movieDTO", movieDTO);
        request.setAttribute("sessions", sessions);
        request.getRequestDispatcher("pages/common/movie.jsp").forward(request, response);
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
