package ua.org.oa.snape28.controllers.User;

import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.dto.UserDTO;
import ua.org.oa.snape28.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a controller which process buy of ticket.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "BuyTicketServlet", urlPatterns = "/buyTicket")
public class BuyTicketServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        TicketDTO ticketDTO = TicketServiceImpl.getInstance().getById(
                Integer.parseInt(request.getParameter("id")));

        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        if (userDTO.getRole().getRole().equals("admin")) {
            request.getSession().setAttribute("warn_message", "Admin can not buy a ticket!");
            request.getRequestDispatcher("pages/admin/admin.jsp").forward(request, response);
        } else {
            ticketDTO.setUser(userDTO);
            TicketServiceImpl.getInstance().update(ticketDTO);
            request.setAttribute("ticketDTO", ticketDTO);
            request.getRequestDispatcher("pages/common/approwTicket.jsp").forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
