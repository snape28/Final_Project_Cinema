package ua.org.oa.snape28.controllers.common;



import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a controller which process output page of all tickets which exist in movie session.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "TicketListServlet", urlPatterns = "/tickets")
public class TicketListServlet extends HttpServlet {
    /**
     * Creates array rows and places.
     * @param ticketDTOList List of all tickets for movie session.
     */
    private void createArrayRowsAndPlaces(List<TicketDTO> ticketDTOList) {
        List<ArrayList<TicketDTO>> listOfLists = new ArrayList<>();
        for (int i = 1; i < ticketDTOList.size(); i++) {
            for (TicketDTO ticket : ticketDTOList) {
                if (ticket.getPlace().getRow() == i) {
                    if (!listOfLists.contains(i)) {
                        listOfLists.add(i, new ArrayList<>());
                        listOfLists.get(i).add(ticket);
                    } else {
                        listOfLists.get(i).add(ticket);
                    }
                }
            }
        }
        TicketDTO [][] ticketTable = new TicketDTO[listOfLists.size()][];
        int i = 0;
        for (List<TicketDTO> nestedList : listOfLists) {
            ticketTable[i++] = nestedList.toArray(new TicketDTO[nestedList.size()]);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ticketDTOList", prepareHallViewModel(Integer.parseInt(request.getParameter("id"))));

        request.getRequestDispatcher("pages/common/tickets.jsp").forward(request, response);

    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Prepares a hall view model for output tickets on page.
     * @param sessionId Id session.
     * @return list of list(places).
     */
    private List<List<TicketDTO>> prepareHallViewModel(int sessionId){
        List<List<TicketDTO>> result = new ArrayList<>();
        List<TicketDTO> tickets = TicketServiceImpl.getInstance().getAllByFieldId("session_id",sessionId);
        tickets.sort(new Comparator<TicketDTO>() {
            @Override
            public int compare(TicketDTO ticketDTO, TicketDTO t1) {
                if (ticketDTO.getPlace().getRow() > t1.getPlace().getRow()){
                    return 1;
                } else if (ticketDTO.getPlace().getRow() < t1.getPlace().getRow()){
                    return -1;
                } else if (ticketDTO.getPlace().getPlaceNumber() > t1.getPlace().getPlaceNumber()){
                    return 1;
                } else {return -1;}
            }

        });

        int counter = 0;
        List<TicketDTO> row = null;
        for (TicketDTO ticket:tickets) {

            if (counter != ticket.getPlace().getRow()){
                counter++;
                row = new ArrayList<>();
                result.add(row);
            }
            row.add(ticket);
        }

        return result;
    }
}