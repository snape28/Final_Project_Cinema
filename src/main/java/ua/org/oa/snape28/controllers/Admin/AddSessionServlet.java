package ua.org.oa.snape28.controllers.Admin;



import ua.org.oa.snape28.dto.PlaceDTO;
import ua.org.oa.snape28.dto.SessionDTO;
import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Represents a controller which process add movies sessions page by admin.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "AddSessionServlet", urlPatterns = "/admin_add_session")
public class AddSessionServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int filmId = Integer.parseInt(request.getParameter("movie_id"));
        int hallId = Integer.parseInt(request.getParameter("hall_id"));
        int price = Integer.parseInt(request.getParameter("ticket_price"));
        request.setCharacterEncoding("UTF-8");

        boolean isDateValid = true;

        try {
            Timestamp startDate = Timestamp.valueOf(request.getParameter("session_start"));

            SessionDTO session = new SessionDTO();
            session.setFilm(MovieServiceImpl.getInstance().getById(filmId));
            session.setHalls(HallsServiseImpl.getInstance().getById(hallId));
            session.setTimeOfSession(startDate);
            session.setId(SessionServiseImpl.getInstance().save(session));
            addTicketsForSession(session,hallId,price);

            request.getSession().setAttribute("message", "New session was succesfully added.");
        } catch (Exception e) {
            request.getSession().setAttribute("message", "Invalid session information.");
        }
        response.sendRedirect("admin_sessions");
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);

    }

    /**
     * Adds tickets for session after session was added by admin.
     * @param session Movies session which was added.
     * @param hallId Hall in which movie session was added.
     * @param price Price of tickets for movie session.
     */
    private void addTicketsForSession(SessionDTO session, int hallId, int price) {
        List<PlaceDTO> places = PlaceServiceImpl.getInstance().getAllByFieldId("hall_id", hallId);

        for (PlaceDTO place: places) {
            TicketServiceImpl.getInstance().save(new TicketDTO(session,
                    place, null, price));
        }
    }
}
