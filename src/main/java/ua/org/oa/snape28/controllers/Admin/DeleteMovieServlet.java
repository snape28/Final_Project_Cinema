package ua.org.oa.snape28.controllers.Admin;



import ua.org.oa.snape28.dto.SessionDTO;
import ua.org.oa.snape28.service.impl.MovieServiceImpl;
import ua.org.oa.snape28.service.impl.SessionServiseImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process deleting movie from cinema by admin.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "DeleteMovieServlet",urlPatterns = "/admin_delete_movie")
public class DeleteMovieServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int movieId = Integer.parseInt(request.getParameter("id"));
        List<SessionDTO> sessions = SessionServiseImpl.getInstance().getAllByFieldId("movie_id", movieId);
        request.setCharacterEncoding("UTF-8");
        if (sessions.isEmpty()) {
            MovieServiceImpl.getInstance().delete(movieId);
            request.getSession().setAttribute("message", "Movie was removed");
        } else {
            request.getSession().setAttribute("message", "Move could be removed because it still in use with sessions.");
        }
        response.sendRedirect("admin_movies");
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
