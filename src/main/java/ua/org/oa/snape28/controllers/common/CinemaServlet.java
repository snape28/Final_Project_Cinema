package ua.org.oa.snape28.controllers.common;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a controller which process main cinema page.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "CinemaServlet", urlPatterns = {"/", "/cinema"})
public class CinemaServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/pages/common/cinema.jsp").forward(request, response);
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}


