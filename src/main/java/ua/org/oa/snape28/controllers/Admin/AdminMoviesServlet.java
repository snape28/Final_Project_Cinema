package ua.org.oa.snape28.controllers.Admin;


import ua.org.oa.snape28.dto.MovieDTO;
import ua.org.oa.snape28.service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process page with all movies.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "AdminMoviesServlet", urlPatterns = "/admin_movies")
public class AdminMoviesServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("movieDTOList", movieDTOList);
        request.getRequestDispatcher("/pages/admin/admin_movies.jsp").include(request, response);
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
