package ua.org.oa.snape28.controllers.common;



import ua.org.oa.snape28.dto.SessionDTO;
import ua.org.oa.snape28.service.impl.SessionServiseImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process output page of all of movies sessions which exist in cinema.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "SessionsServlet", urlPatterns = "/sessions")
public class SessionsServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<SessionDTO> sessionDTOList = SessionServiseImpl.getInstance().getAll();
        request.setAttribute("sessionDTOList", sessionDTOList);

        request.getRequestDispatcher("/pages/common/sessions_list.jsp").forward(request, response);
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
