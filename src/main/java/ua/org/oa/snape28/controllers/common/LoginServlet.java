package ua.org.oa.snape28.controllers.common;



import ua.org.oa.snape28.dto.UserDTO;
import ua.org.oa.snape28.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a controller which process log in page.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        UserDTO userDTO = UserServiceImpl.getInstance().getByLogin(login);

        if (userDTO != null && userDTO.getPassword().equals(password)) {

            request.getSession().setAttribute("user", userDTO);

            if (request.getSession().getAttribute("URL") != null) {
                request.getRequestDispatcher(request.getSession().getAttribute("URL").toString()).forward(request, response);
            } else {
                request.getRequestDispatcher("pages/common/cinema.jsp").forward(request, response);
            }
        } else {
            request.getSession().setAttribute("message", "Wrong user's name or password");
            request.getRequestDispatcher("pages/common/login.jsp").forward(request, response);
        }
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
