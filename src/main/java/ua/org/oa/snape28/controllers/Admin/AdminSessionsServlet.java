package ua.org.oa.snape28.controllers.Admin;



import ua.org.oa.snape28.dto.HallDTO;
import ua.org.oa.snape28.dto.MovieDTO;
import ua.org.oa.snape28.dto.SessionDTO;
import ua.org.oa.snape28.service.impl.HallsServiseImpl;
import ua.org.oa.snape28.service.impl.MovieServiceImpl;
import ua.org.oa.snape28.service.impl.SessionServiseImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process page with all movies sessions.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "AdminSessionsServlet", urlPatterns = "/admin_sessions")
public class AdminSessionsServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<SessionDTO> sessionsDTOList = SessionServiseImpl.getInstance().getAll();
        request.setCharacterEncoding("UTF-8");

        request.setAttribute("sessionsDTOList", sessionsDTOList);

        List<MovieDTO> movies = MovieServiceImpl.getInstance().getAll();
        request.setAttribute("moviesDTOList", movies);
        List<HallDTO> halls = HallsServiseImpl.getInstance().getAll();
        request.setAttribute("hallsDTOList", halls);

        request.getRequestDispatcher("/pages/admin/admin_sessions.jsp").include(request,response);
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
