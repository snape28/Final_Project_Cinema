package ua.org.oa.snape28.controllers.Admin;



import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.service.impl.SessionServiseImpl;
import ua.org.oa.snape28.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Represents a controller which process deleting movie sessions from cinema by admin.
 *
 * @author Svyatoslav Kotlyar.
 */
@WebServlet(name = "RemoveSessionServlet", urlPatterns = "/admin_delete_session")
public class RemoveSessionServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int sessionId = Integer.parseInt(request.getParameter("id"));
        request.setCharacterEncoding("UTF-8");
        removeSessionTickets(sessionId);
        SessionServiseImpl.getInstance().delete(sessionId);

        request.getSession().setAttribute("message", "Session was removed");
        response.sendRedirect("admin_sessions");
    }
    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    /**
     * Removes tickets for session before removing movie session.
     * @param sessionId Session id tickets of each need to remove.
     */
    private void removeSessionTickets(int sessionId) {
        List<TicketDTO> tickets = TicketServiceImpl.getInstance().getAllByFieldId("session_id",sessionId);
        for (TicketDTO ticket : tickets) {
            TicketServiceImpl.getInstance().delete(ticket.getId());
        }
    }
}
