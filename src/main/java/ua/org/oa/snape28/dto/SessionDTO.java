package ua.org.oa.snape28.dto;


import lombok.Data;
import ua.org.oa.snape28.model.Entity;

import java.sql.Timestamp;

/**
 * Session DTO.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class SessionDTO extends Entity<Integer> {
    /**
     * Object film.
     */
    private MovieDTO film;
    /**
     * Session time.
     */
    private Timestamp timeOfSession;

    /**
     * Object hall.
     */
    private HallDTO halls;

    /**
     * Constructor.
     *
     * @param film          Object film.
     * @param timeOfSession Session time.
     * @param halls         Object hall.
     */
    public SessionDTO(MovieDTO film, Timestamp timeOfSession, HallDTO halls) {
        this.film = film;
        this.timeOfSession = timeOfSession;
        this.halls = halls;
    }
    /**
     * Constructor.
     */
    public SessionDTO() {
    }

}

