package ua.org.oa.snape28.dto;


import lombok.Data;
import ua.org.oa.snape28.model.Entity;

/**
 * Ticket DTO.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class TicketDTO extends Entity<Integer> {
    /**
     * Film session.
     */
    private SessionDTO session;
    /**
     * Place.
     */
    private PlaceDTO place;
    /**
     * User which buy ticket.
     */
    private UserDTO user;
    /**
     * Price of ticket.
     */
    private int price;

    /**
     * Constructor.
     *
     * @param session Film session.
     * @param places  Place.
     * @param user    User which buy ticket.
     * @param price   Price of ticket.
     */
    public TicketDTO(SessionDTO session, PlaceDTO places, UserDTO user, int price) {
        this.session = session;
        this.place = places;
        this.user = user;
        this.price = price;
    }

    /**
     * Constructor.
     */
    public TicketDTO() {
    }
}
