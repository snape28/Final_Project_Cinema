package ua.org.oa.snape28.dto;


import lombok.Data;
import ua.org.oa.snape28.model.Entity;

/**
 * Film DTO
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class MovieDTO extends Entity<Integer> {
    /**
     * Name of movie.
     */
    private String name;
    /**
     * Description of movie.
     */
    private String description;
    /**
     * Duration of movie.
     */
    private int duration;

    /**
     * Constructor.
     */
    public MovieDTO() {
    }

    /**
     * Constructor.
     *
     * @param name        Name of movie.
     * @param duration    Duration of movie.
     * @param description Description of movie.
     */
    public MovieDTO(String name, int duration, String description) {
        this.name = name;
        this.description = description;
        this.duration = duration;
    }
}
