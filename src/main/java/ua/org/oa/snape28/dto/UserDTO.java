package ua.org.oa.snape28.dto;


import lombok.Data;
import ua.org.oa.snape28.model.Entity;

/**
 * User DTO.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class UserDTO extends Entity<Integer> {
    /**
     * User login
     */
    private String login;
    /**
     * User password.
     */
    private String password;
    /**
     * classes.Users mail.
     */
    private String mail;
    /**
     * Role of user.
     */
    private RoleDTO role;

    /**
     * Constructor.
     *
     * @param login    classes.Users LoginServlet.
     * @param password classes.Users password.
     * @param mail     classes.Users mail.
     * @param role     RoleDTO.
     */
    public UserDTO(String login, String password, String mail, RoleDTO role) {
        this.login = login;
        this.password = password;
        this.mail = mail;
        this.role = role;
    }
    public UserDTO() {
    }

    /**
     * Sets role.
     * @param role role.
     */
    public void setRole(RoleDTO role) {
        this.role = role;
    }

}
