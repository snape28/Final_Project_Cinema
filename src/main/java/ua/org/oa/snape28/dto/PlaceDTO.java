package ua.org.oa.snape28.dto;


import lombok.Data;
import ua.org.oa.snape28.model.Entity;
import ua.org.oa.snape28.model.Halls;

/**
 * Place DTO.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class PlaceDTO extends Entity<Integer> {
    /**
     * Hall.
     */
    private Halls hall;
    /**
     * Row number.
     */
    private int row;
    /**
     * Row number.
     */
    private int placeNumber;

    /**
     * Constructor.
     *
     * @param hall        Hall.
     * @param row         Row number.
     * @param placeNumber Row number.
     */
    public PlaceDTO(Halls hall, int row, int placeNumber) {
        this.hall = hall;
        this.row = row;
        this.placeNumber = placeNumber;
    }

    /**
     * Constructor.
     */
    public PlaceDTO() {
    }

}
