package ua.org.oa.snape28.dto;

import lombok.Data;
import ua.org.oa.snape28.model.Entity;

/**
 * Hall DTO.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class HallDTO extends Entity<Integer> {
    /**
     * Name of hall.
     */
    private String name;


    /**
     * Constructor.
     *
     * @param name Name of hall.
     */
    public HallDTO(String name) {
        this.name = name;
    }

    /**
     * Constructor.
     */
    public HallDTO() {
    }

}
