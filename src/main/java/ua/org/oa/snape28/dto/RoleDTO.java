package ua.org.oa.snape28.dto;


import lombok.Data;
import ua.org.oa.snape28.model.Entity;

/**
 * Role DTO.
 *
 * @author Svyatoslav Kotlyar.
 */
@Data
public class RoleDTO extends Entity<Integer> {
    /**
     * Name of role.
     */
    private String role;

    /**
     * Constructor.
     */
    public RoleDTO() {
    }

    /**
     * Constructor.
     * @param id role id.
     */
    public RoleDTO(Integer id) {
        if (id == 2) {
            this.role = "customer";
            this.setId(2);
        }
    }

}

