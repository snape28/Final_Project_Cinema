package ua.org.oa.snape28.service.impl;

import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.PlaceDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Places;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * Place service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class PlaceServiceImpl implements Service<Integer, PlaceDTO> {

    private static PlaceServiceImpl service;
    private Dao<Integer, Places> placeDao;
    private BeanMapper beanMapper;

    private PlaceServiceImpl() {
        placeDao = DaoFactory.getInstance().getPlaceDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized PlaceServiceImpl getInstance() {
        if (service == null) {
            service = new PlaceServiceImpl();
        }
        return service;
    }

    @Override
    public List<PlaceDTO> getAll() {
        List<Places> places = placeDao.getAll();
        List<PlaceDTO> placeDTOs = beanMapper.listMapToList(places, PlaceDTO.class);
        return placeDTOs;
    }

    @Override
    public PlaceDTO getById(Integer id) {
        Places places = placeDao.getById(id);
        PlaceDTO placeDTO = beanMapper.singleMapper(places, PlaceDTO.class);
        return placeDTO;
    }

    @Override
    public int save(PlaceDTO entity) {
        Places places = beanMapper.singleMapper(entity, Places.class);
        placeDao.save(places);
        return places.getId();
    }

    @Override
    public void delete(Integer key) {
        placeDao.delete(key);
    }

    @Override
    public void update(PlaceDTO entity) {
        Places places = beanMapper.singleMapper(entity, Places.class);
        placeDao.update(places);
    }

    @Override
    public PlaceDTO getBy(String fieldName, String value) {
        Places places = placeDao.getBy(fieldName, value);
        PlaceDTO placeDTO = beanMapper.singleMapper(places, PlaceDTO.class);
        return placeDTO;
    }

    @Override
    public List<PlaceDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Places> places = placeDao.getAllByFieldId(fieldName, key);
        List<PlaceDTO> placeDTOs = beanMapper.listMapToList(places, PlaceDTO.class);
        return placeDTOs;
    }
}
