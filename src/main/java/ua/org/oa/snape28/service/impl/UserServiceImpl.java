package ua.org.oa.snape28.service.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.UserDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Users;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * User service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class UserServiceImpl implements Service<Integer, UserDTO> {

    private static UserServiceImpl service;
    private Dao<Integer, Users> userDao;
    private BeanMapper beanMapper;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }
        return service;
    }


    @Override
    public List<UserDTO> getAll() {
        List<Users> userss = userDao.getAll();
        List<UserDTO> usersDTO = beanMapper.listMapToList(userss, UserDTO.class);
        return usersDTO;
    }

    @Override
    public UserDTO getById(Integer id) {
        Users users = userDao.getById(id);
        UserDTO userDTO = beanMapper.singleMapper(users, UserDTO.class);
        return userDTO;
    }

    @Override
    public int save(UserDTO entity) {
        Users users = beanMapper.singleMapper(entity, Users.class);
        userDao.save(users);
        return users.getId();
    }

    public UserDTO getByLogin(String login) {
        Users users = userDao.getBy("login", login);
        UserDTO userDTO = beanMapper.singleMapper(users, UserDTO.class);
        return userDTO;
    }

    @Override
    public void delete(Integer key) {
        userDao.delete(key);
    }


    @Override
    public void update(UserDTO entity) {
        Users users = beanMapper.singleMapper(entity, Users.class);
        userDao.update(users);
    }

    @Override
    public UserDTO getBy(String fieldName, String value) {
        Users users = userDao.getBy(fieldName, value);
        UserDTO userDTO = beanMapper.singleMapper(users, UserDTO.class);
        return userDTO;
    }

    @Override
    public List<UserDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Users> userss = userDao.getAllByFieldId(fieldName, key);
        List<UserDTO> usersDTO = beanMapper.listMapToList(userss, UserDTO.class);
        return usersDTO;
    }
}
