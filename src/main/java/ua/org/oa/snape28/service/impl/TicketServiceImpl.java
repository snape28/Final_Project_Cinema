package ua.org.oa.snape28.service.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.TicketDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Tickets;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * Ticket service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class TicketServiceImpl implements Service<Integer, TicketDTO> {

    private static TicketServiceImpl service;
    private Dao<Integer, Tickets> ticketsDao;
    private BeanMapper beanMapper;

    private TicketServiceImpl() {
        ticketsDao = DaoFactory.getInstance().getTicketsDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized TicketServiceImpl getInstance() {
        if (service == null) {
            service = new TicketServiceImpl();
        }
        return service;
    }

    @Override
    public List<TicketDTO> getAll() {
        List<Tickets> ticketss = ticketsDao.getAll();
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(ticketss, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public TicketDTO getById(Integer id) {
        Tickets tickets = ticketsDao.getById(id);
        TicketDTO ticketDTO = beanMapper.singleMapper(tickets, TicketDTO.class);
        return ticketDTO;
    }

    @Override
    public int save(TicketDTO entity) {
        Tickets tickets = beanMapper.singleMapper(entity, Tickets.class);
        ticketsDao.save(tickets);
        return tickets.getId();
    }

    @Override
    public void delete(Integer key) {
        ticketsDao.delete(key);
    }

    @Override
    public void update(TicketDTO entity) {
        Tickets tickets = beanMapper.singleMapper(entity, Tickets.class);
        ticketsDao.update(tickets);
    }

    @Override
    public TicketDTO getBy(String fieldName, String value) {
        Tickets tickets = ticketsDao.getBy(fieldName, value);
        TicketDTO ticketDTO = beanMapper.singleMapper(tickets, TicketDTO.class);
        return ticketDTO;
    }

    @Override
    public List<TicketDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Tickets> ticketss = ticketsDao.getAllByFieldId(fieldName, key);
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(ticketss, TicketDTO.class);
        return ticketDTOs;
    }


}
