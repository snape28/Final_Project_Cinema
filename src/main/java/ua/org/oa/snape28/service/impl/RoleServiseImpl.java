package ua.org.oa.snape28.service.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.RoleDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Role;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * Role service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class RoleServiseImpl implements Service<Integer, RoleDTO> {

    private static RoleServiseImpl service;
    private Dao<Integer, Role> roleDao;
    private BeanMapper beanMapper;

    private RoleServiseImpl() {
        roleDao = DaoFactory.getInstance().getRoleDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized RoleServiseImpl getInstance() {
        if (service == null) {
            service = new RoleServiseImpl();
        }
        return service;
    }

    @Override
    public List<RoleDTO> getAll() {
        List<Role> roles = roleDao.getAll();
        List<RoleDTO> roleDTOs = beanMapper.listMapToList(roles, RoleDTO.class);
        return roleDTOs;
    }

    @Override
    public RoleDTO getById(Integer id) {
        Role role = roleDao.getById(id);
        RoleDTO roleDTO = beanMapper.singleMapper(role, RoleDTO.class);
        return roleDTO;
    }

    @Override
    public int save(RoleDTO entity) {
        Role role = beanMapper.singleMapper(entity, Role.class);
        roleDao.save(role);
        return role.getId();
    }

    @Override
    public void delete(Integer key) {
        roleDao.delete(key);
    }

    @Override
    public void update(RoleDTO entity) {
        Role role = beanMapper.singleMapper(entity, Role.class);
        roleDao.update(role);
    }

    @Override
    public RoleDTO getBy(String fieldName, String value) {
        Role role = roleDao.getBy(fieldName, value);
        RoleDTO roleDTO = beanMapper.singleMapper(role, RoleDTO.class);
        return roleDTO;
    }

    @Override
    public List<RoleDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Role> roles = roleDao.getAllByFieldId(fieldName, key);
        List<RoleDTO> roleDTOs = beanMapper.listMapToList(roles, RoleDTO.class);
        return roleDTOs;
    }
}
