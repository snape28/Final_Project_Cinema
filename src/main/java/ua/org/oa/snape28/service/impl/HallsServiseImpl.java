package ua.org.oa.snape28.service.impl;

import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.HallDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Halls;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * Hall service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class HallsServiseImpl implements Service<Integer, HallDTO> {

    private static HallsServiseImpl service;
    private Dao<Integer, Halls> hallsDao;
    private BeanMapper beanMapper;

    private HallsServiseImpl() {
        hallsDao = DaoFactory.getInstance().getHallsDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized HallsServiseImpl getInstance() {
        if (service == null) {
            service = new HallsServiseImpl();
        }
        return service;
    }

    @Override
    public List<HallDTO> getAll() {
        List<Halls> halls = hallsDao.getAll();
        List<HallDTO> hallDTOs = beanMapper.listMapToList(halls, HallDTO.class);
        return hallDTOs;
    }

    @Override
    public HallDTO getById(Integer id) {
        Halls halls = hallsDao.getById(id);
        HallDTO hallDTO = beanMapper.singleMapper(halls, HallDTO.class);
        return hallDTO;
    }

    @Override
    public int save(HallDTO entity) {
        Halls halls = beanMapper.singleMapper(entity, Halls.class);
        hallsDao.save(halls);
        return halls.getId();
    }

    @Override
    public void delete(Integer key) {
        hallsDao.delete(key);
    }

    @Override
    public void update(HallDTO entity) {
        Halls halls = beanMapper.singleMapper(entity, Halls.class);
        hallsDao.update(halls);
    }

    @Override
    public HallDTO getBy(String fieldName, String value) {
        Halls halls = hallsDao.getBy(fieldName, value);
        HallDTO hallDTO = beanMapper.singleMapper(halls, HallDTO.class);
        return hallDTO;
    }

    @Override
    public List<HallDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Halls> halls = hallsDao.getAllByFieldId(fieldName, key);
        List<HallDTO> hallDTOs = beanMapper.listMapToList(halls, HallDTO.class);
        return hallDTOs;
    }
}
