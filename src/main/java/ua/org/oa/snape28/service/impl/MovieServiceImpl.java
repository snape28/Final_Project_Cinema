package ua.org.oa.snape28.service.impl;


import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.MovieDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Movies;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * Movie service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class MovieServiceImpl implements Service<Integer, MovieDTO> {

    private static MovieServiceImpl service;
    private Dao<Integer, Movies> movieDao;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao = DaoFactory.getInstance().getMovieDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieServiceImpl getInstance() {
        if (service == null) {
            service = new MovieServiceImpl();
        }
        return service;
    }

    @Override
    public List<MovieDTO> getAll() {
        List<Movies> movies = movieDao.getAll();
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies, MovieDTO.class);
        return movieDTOs;
    }

    @Override
    public int save(MovieDTO movieDto) {
        Movies movies = beanMapper.singleMapper(movieDto, Movies.class);
        movieDao.save(movies);
        return movies.getId();
    }

    @Override
    public MovieDTO getById(Integer id) {
        Movies movies = movieDao.getById(id);
        MovieDTO movieDTO = beanMapper.singleMapper(movies, MovieDTO.class);
        return movieDTO;
    }

    @Override
    public void delete(Integer key) {
        movieDao.delete(key);
    }

    @Override
    public void update(MovieDTO entity) {
        Movies movies = beanMapper.singleMapper(entity, Movies.class);
        movieDao.update(movies);
    }

    @Override
    public MovieDTO getBy(String fieldName, String value) {
        Movies movies = movieDao.getBy(fieldName, value);
        MovieDTO movieDTO = beanMapper.singleMapper(movies, MovieDTO.class);
        return movieDTO;
    }

    @Override
    public List<MovieDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Movies> movies = movieDao.getAllByFieldId(fieldName, key);
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies, MovieDTO.class);
        return movieDTOs;
    }
}
