package ua.org.oa.snape28.service.impl;

import ua.org.oa.snape28.dao.DaoFactory;
import ua.org.oa.snape28.dao.api.Dao;
import ua.org.oa.snape28.dto.SessionDTO;
import ua.org.oa.snape28.mapper.BeanMapper;
import ua.org.oa.snape28.model.Sessions;
import ua.org.oa.snape28.service.api.Service;

import java.util.List;

/**
 * Session service implementation.
 *
 * @author Svyatoslav Kotlyar.
 */
public class SessionServiseImpl implements Service<Integer, SessionDTO> {

    private static SessionServiseImpl service;
    private Dao<Integer, Sessions> sessionsDao;
    private BeanMapper beanMapper;

    private SessionServiseImpl() {
        sessionsDao = DaoFactory.getInstance().getSessionDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized SessionServiseImpl getInstance() {
        if (service == null) {
            service = new SessionServiseImpl();
        }
        return service;
    }

    @Override
    public List<SessionDTO> getAll() {
        List<Sessions> sessions = sessionsDao.getAll();
        List<SessionDTO> sessionDTOs = beanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    @Override
    public SessionDTO getById(Integer id) {
        Sessions sessions = sessionsDao.getById(id);
        SessionDTO sessionDTO = beanMapper.singleMapper(sessions, SessionDTO.class);
        return sessionDTO;
    }

    @Override
    public int save(SessionDTO entity) {
        Sessions sessions = beanMapper.singleMapper(entity, Sessions.class);
        sessionsDao.save(sessions);
        return sessions.getId();
    }

    @Override
    public void delete(Integer key) {
        sessionsDao.delete(key);
    }

    @Override
    public void update(SessionDTO entity) {
        Sessions sessions = beanMapper.singleMapper(entity, Sessions.class);
        sessionsDao.update(sessions);
    }

    @Override
    public SessionDTO getBy(String fieldName, String value) {
        Sessions sessions = sessionsDao.getBy(fieldName, value);
        SessionDTO sessionDTO = beanMapper.singleMapper(sessions, SessionDTO.class);
        return sessionDTO;
    }

    @Override
    public List<SessionDTO> getAllByFieldId(String fieldName, Integer key) {
        List<Sessions> sessions = sessionsDao.getAllByFieldId(fieldName, key);
        List<SessionDTO> sessionDTOs = beanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }
}
