package ua.org.oa.snape28.service.api;

import java.util.List;

/**
 * Service interface.
 *
 * @author Svyatoslav Kotlyar.
 */
public interface Service<K, T> {
    /**
     * Gets all objects from database from table.
     *
     * @return List of objects.
     */
    List<T> getAll();

    /**
     * Gets object by id from database.
     *
     * @param id id of object.
     * @return Object by id.
     */
    T getById(K id);

    /**
     * Saves object in database.
     *
     * @param entity Entity which need to save in database.
     */
    int save(T entity);

    /**
     * Removes object from database by id.
     *
     * @param key id of object.
     */
    void delete(K key);

    /**
     * Updates object in database.
     *
     * @param entity Entity which need to update in database.
     */
    void update(T entity);

    /**
     * Gets object by value of field from database.
     *
     * @param fieldName Name of field by which need search.
     * @param value     Value of field by which need search.
     * @return Object by value of field.
     */
    T getBy(String fieldName, String value);

    /**
     * Gets all objects from database by value of field.
     *
     * @param fieldName Name of field by which need search.
     * @param key       Value of field by which need search.
     * @return List of objects.
     */
    List<T> getAllByFieldId(String fieldName, K key);

}
