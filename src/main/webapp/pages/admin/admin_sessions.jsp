<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 14:52
  To change this template use File | Settings | File Templates.

  <c:forEach items="${sessionDTOList}" var="sessions">
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin page|Movies session</title>

</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.servletContext.contextPath}/cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="admin">Admin page</a></li>
                <li><a href="admin_sessions">Add session</a></li>
                <li><a href="admin_movies">Add movie</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="page-header">
    <h1>All movie session</h1>
</div>


<div class="btn-group" role="group" aria-label="Basic example">

    <button type="button" class="btn btn-secondary">
        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
    </button>


    <button type="button" class="btn btn-secondary">Today</button>
    <button type="button" class="btn btn-secondary">Tomorrow</button>
</div>

<h2><c:out value="${sessionScope.message}"/></h2>

<table class="table table-striped">
    <tr>
        <th>Movie title</th>
        <th>Time</th>
        <th></th>
    </tr>

    <c:forEach items="${sessionsDTOList}" var="sessions">
        <tr>
            <td>
                <a href="${pageContext.servletContext.contextPath}/movie?id=${sessions.film.id}">${sessions.film.name}</a><br/>
            </td>
            <td>
                <a href="${pageContext.servletContext.contextPath}/tickets?id=${sessions.id}">
                    <fmt:formatDate value="${sessions.timeOfSession}" pattern="dd/MM/yyyy HH:mm"/></a><br/>
            </td>
            <td>
                <a href="${pageContext.servletContext.contextPath}/admin_delete_session?id=${sessions.id}"
                   class="btn btn-default">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Add Session</h1>
            </div>
            <div class="panel-body">
                <form name="addSessionForm" method="post"
                      action="/admin_add_session">
                    <div class="form-group">
                        <label>Movie</label>
                        <select name="movie_id">
                            <c:forEach items="${moviesDTOList}" var="movie">
                                <option value="${movie.id}">${movie.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Hall</label>
                        <select name="hall_id">
                            <c:forEach items="${hallsDTOList}" var="hall">
                                <option value="${hall.id}">${hall.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Start time</label>
                        <input class="form-control" id="duration" name="session_start"
                               placeholder="yyyy-MM-dd HH:mm:ss">
                    </div>
                    <div class="form-group">
                        <label>Price of tickets</label>
                        <input class="form-control" id="ticket_price" name="ticket_price"
                               placeholder="Enter: 100, 80, 90 ...">
                    </div>
                    <button type="submit" class="btn btn-default">Add session</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>