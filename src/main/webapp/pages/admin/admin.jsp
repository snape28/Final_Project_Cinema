<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin home</title>

</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="admin">Admin page</a></li>
                <li><a href="admin_sessions">Add session</a></li>
                <li><a href="admin_movies">Add movie</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</nav>
<c:choose>

    <c:when test="${empty sessionScope.warn_message}">
    </c:when>
    <c:otherwise>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Alarm</h3>
            </div>
            <div class="panel-body">
                <h1><c:out value="${sessionScope.warn_message}"/></h1>
            </div>
        </div>
    </c:otherwise>
</c:choose>

</body>
</html>

