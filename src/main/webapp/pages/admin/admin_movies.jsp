<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin page|Movies list</title>



</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.servletContext.contextPath}/cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="admin">Admin page</a></li>
                <li><a href="admin_sessions">Add session</a></li>
                <li><a href="admin_movies">Add movie</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<h2><c:out value="${sessionScope.message}"/></h2>
<table class="table table-hover">

    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Duration</th>
        <th>Action</th>
    </tr>

    <c:forEach items="${movieDTOList}" var="movie">
        <tr>
            <td>${movie.name}</td>
            <td>${movie.description}</td>
            <td>${movie.duration} min</td>
            <td><a href="${pageContext.servletContext.contextPath}/admin_delete_movie?id=${movie.id}" class="btn btn-default">Delete</a></td>
        </tr>
    </c:forEach>

</table>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Add movie</h1>
            </div>
            <div class="panel-body">
                <form name="AddMovieForm" method="post"
                      action="admin_add_movie">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" id="name" name="name"
                               placeholder="Enter movie name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input class="form-control" id="description" name="description"
                               placeholder="Enter information about movie">
                    </div>
                    <div class="form-group">
                        <label>Duration</label>
                        <input class="form-control" id="duration" name="duration"
                               placeholder="Movie duration in minutes">
                    </div>
                    <button type="submit" class="btn btn-default">Add movie</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>