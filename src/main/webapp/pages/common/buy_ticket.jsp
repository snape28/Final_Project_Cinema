<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cinema|Buy ticket</title>


    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<script src="../resources/js/bootstrap.min.js"></script>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="sessions">Movie sessions</a></li>
                <li><a href="movies">Movies</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </form>
            <ul class="nav navbar-nav navbar-right">

                <c:choose>
                    <c:when test="${empty user}">
                        <li><a href="signup">Sign up</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">Account<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="login">Log in</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:when>


                    <c:otherwise>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="usersTicket">My tickets</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>

            </ul>
        </div>
    </div>
</nav>


<h2>Check parameters before buy ticket:</h2>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-body">
            <p class="text-center">
                <c:out value="Movie title: ${ticketDTO.session.film.name}"/> <br/>
                Time of movie: <fmt:formatDate value="${ticketDTO.session.timeOfSession}" pattern="dd/MM/yyyy HH:mm"/>
                <br/>
                <c:out value="Hall: ${ticketDTO.session.halls.name}"/><br/>
                <c:out value="Place: ${ticketDTO.place.placeNumber}"/><br/>
                <c:out value="Row: ${ticketDTO.place.row}"/><br/>
                <c:out value="Price: ${ticketDTO.price}"/><br/>
            </p>
            <form name="buyTicket" method="post"
                  action="buyTicket?id=${ticketDTO.id}">
                <button type="submit" class="btn btn-default">Buy</button>
            </form>

        </div>
    </div>
</div>
</body>
</html>
