<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 19:02
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About movie</title>

</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="sessions">Movie sessions</a></li>
                <li><a href="movies">Movies</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                <c:choose>
                    <c:when test="${empty user}">
                        <li><a href="signup">Sign up</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">Account<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="login">Log in</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:when>


                    <c:otherwise>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="usersTicket">My ticket</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>

            </ul>
        </div>
    </div>
</nav>

<table class="table table-striped">
    <h1><c:out value="${movieDTO.name}" default="err"/></h1><br/>
    <c:out value="Description: ${movieDTO.description}" default="err"/><br/>
    <c:out value="Duration: ${movieDTO.duration} min." default="err"/><br/>
</table>

<div class="list-group">
    <a class="list-group-item active">All sessions for this movie: </a>
    <c:forEach items="${sessions}" var="session">
        <a href="tickets?id=${session.id}" class="list-group-item"><fmt:formatDate value="${session.timeOfSession}"
                                                                                   pattern="dd/MM/yyyy HH:mm"/></a>
    </c:forEach>
</div>

</body>
</html>
