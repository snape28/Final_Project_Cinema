<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 12:53
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Your tickets</title>
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="sessions">Movie sessions</a></li>
                <li><a href="movies">Movies</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <c:choose>
                    <c:when test="${empty user}">
                        <li><a href="signup">Sign up</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">Account<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="login">Log in</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:when>


                    <c:otherwise>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="usersTicket">My tickets</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>

            </ul>
        </div>
    </div>
</nav>

<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">All your tickets</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">

            <tr>
                <th>Name</th>
                <th>Date and time of movie</th>
                <th>Hall</th>
                <th>Row</th>
                <th>Place</th>
                <th>Price</th>
                <th>Action</th>
            </tr>

            <c:forEach items="${userTickets}" var="ticket">
                        <tr>
                            <td><a href="movie?id=${ticket.session.film.id}"
                                   class="btn btn-default">${ticket.session.film.name}</a></td>
                            <td>${ticket.session.timeOfSession}</td>
                            <td>${ticket.place.hall.name}</td>
                            <td>${ticket.place.row}</td>
                            <td>${ticket.place.placeNumber}</td>
                            <td>${ticket.price}</td>
                            <td>
                                <button type="button" class="btn btn-default btn-lg">
                                    <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
            </c:forEach>
        </table>
    </div>
</div>

</body>
</html>
