<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign up</title>

</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="sessions">Movie sessions</a></li>
                <li><a href="movies">Movies</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <c:choose>
                    <c:when test="${empty user}">
                        <li><a href="signup">Sign up</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">Account<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="login">Log in</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:when>


                    <c:otherwise>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="usersTicket">My ticket</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>

            </ul>
        </div>
    </div>
</nav>


        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h1>Registration form</h1>
                        <c:out value="${sessionScope.message}"/></div>
                    <div class="panel-body">
                        <form name="registrationForm" method="post"
                              action="${pageContext.servletContext.contextPath}/signup">
                            <div class="form-group">
                                <label for="login">User name</label>
                                <input type="text" class="form-control" id="login" name="login"
                                       placeholder="Enter user name">
                            </div>

                            <div class="form-group">
                                <label for="mail">Email</label>
                                <input type="email" class="form-control" id="mail" name="mail"
                                       placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="Enter password">
                            </div>

                            <button type="submit" class="btn btn-default">Sign up</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

</body>
</html>