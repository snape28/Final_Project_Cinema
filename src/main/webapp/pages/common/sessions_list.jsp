<%--
  Created by IntelliJ IDEA.
  User: Svyatoslav Kotlyar.
  Date: 03.05.17
  Time: 14:52
  To change this template use File | Settings | File Templates.

  <c:forEach items="${sessionDTOList}" var="sessions">
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>All sessions</title>

</head>
<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="cinema">Cinema</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="sessions">Movie sessions</a></li>
                <li><a href="movies">Movies</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <c:choose>
                    <c:when test="${empty user}">
                        <li><a href="signup">Sign up</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">Account<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="login">Log in</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:when>


                    <c:otherwise>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><c:out value="Hello, ${user.login}"/> <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="usersTicket">My ticket</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>

            </ul>
        </div>
    </div>
</nav>

<div class="page-header">
    <h1>All movie session</h1>
</div>


<div class="btn-group" role="group" aria-label="Basic example">

    <button type="button" class="btn btn-secondary">
        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
    </button>


    <button type="button" class="btn btn-secondary">Today</button>
    <button type="button" class="btn btn-secondary">Tomorrow</button>
</div>


<table class="table table-striped">
    <tr>
        <th>Movie title</th>
        <th>Time</th>
    </tr>

    <c:forEach items="${sessionDTOList}" var="sessions">
        <tr>
            <td>
                <a href="movie?id=${sessions.film.id}">${sessions.film.name}</a><br/>
            </td>
            <td>
                <a href="tickets?id=${sessions.id}">
                    <fmt:formatDate value="${sessions.timeOfSession}" pattern="dd/MM/yyyy HH:mm"/></a><br/>
            </td>
        </tr>
    </c:forEach>
</table>


</body>
</html>